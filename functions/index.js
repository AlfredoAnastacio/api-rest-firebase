const functions = require("firebase-functions");

const admin = require('firebase-admin');

var serviceAccount = require("./permissions.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://laravel-auth.firebaseio.com"
});

const express = require('express');
const app = express();
const db = admin.firestore();

const cors = require('cors');
app.use( cors( { origin:true } ) );

// Endpoints
app.get('/hello-world', (req, res) => {
    return res.status(200).send('Hello World!');
});

//Create
app.post('/api/users/v1/create', (req, res) => {
    (async () => {
        try {
            await db.collection('usuarios').doc('/' + req.body.id + '/')
            .create({
                name: req.body.name,
                genero: req.body.genero,
                edad: req.body.edad
            })
            
            return res.status(200).send();

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
});

//Read a specific user data based on id
app.get('/api/users/v1/read/:id', (req, res) => {
    (async () => {
        try {
            const document = db.collection('usuarios').doc(req.params.id);
            let user = await document.get();
            let response = user.data();
            
            return res.status(200).send(response);

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
});

// Read all users
app.get('/api/users/v1/read', (req, res) => {
    (async () => {
        try {
            
            let query = db.collection('usuarios');
            let response = [];

            await query.get().then(querySnapshot => {
                let docs =  querySnapshot.docs;

                for (let doc of docs) {
                    const selectedItem = {
                        id : doc.id,
                        name: doc.data().name,
                        genero: doc.data().genero,
                        edad: doc.data().edad
                    };
                    response.push(selectedItem);
                }
                return response;
            })
            
            return res.status(200).send(response);

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
});

//Update a user by id
app.put('/api/users/v1/update/:id', (req, res) => {
    (async () => {
        try {
            
            const document = db.collection('usuarios').doc(req.params.id);

            await document.update({
                name: req.body.name,
                genero: req.body.genero,
                edad: req.body.edad
            });
            
            return res.status(200).send();

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
});

//Delete user
app.delete('/api/users/v1/delete/:id', (req, res) => {
    (async () => {
        try {
            
            const document = db.collection('usuarios').doc(req.params.id);
            await document.delete();
            
            return res.status(200).send("Usuario eliminado correctamente.");

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
});

//  Export the API to Firebase Cloud Functions
exports.app = functions.https.onRequest(app);